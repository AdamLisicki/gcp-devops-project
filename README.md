# Docker flask application

- This application is written in Python
- It shall be deployed on GKE

## Sprints
- #### Sprint-01:
    - setup GitHub repo for this project
    - create flask app
    - write Dockerfile
    - test app locally
